const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');

const taskRouter = require('./routes/TaskRouter');
const projectRouter = require('./routes/ProjectRouter');
const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, 'public')));

app.use('/task', taskRouter);
app.use('/project', projectRouter);

const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Database Connected Successfully!!");
}).catch(err => {
    console.log('Could not connect to the database', err);
    process.exit();
});

module.exports = app;
