const express = require('express')
const ProjectController = require("../controllers/ProjectController");
const router = express.Router();

router.get('/:id', ProjectController.findOne);
router.post('/:id', ProjectController.update);

module.exports = router
