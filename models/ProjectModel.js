const mongoose = require('mongoose');
const {Schema} = require("mongoose");

let schema = new Schema({
    tasks: [{ type: Schema.Types.ObjectId, ref: 'Task' }],
    timestamp: Date,
});

let project = new mongoose.model('Project', schema);
module.exports = project;
