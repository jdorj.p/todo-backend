let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'created'
    },
    timestamp: Date,
    projectId: String,
});

let task = new mongoose.model('Task', schema);
module.exports = task;
