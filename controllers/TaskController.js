const TaskModel = require('../models/TaskModel');
const ProjectModel = require('../models/ProjectModel');

// Create and save a new task
exports.create = async (req, res) => {

    if (!req.body.name) {
        res.status(400).send({ message: "Content can not be empty!" });
    }

    let project

    if(req.body.projectId){

        project = await ProjectModel.findById(req.body.projectId);

    }

    if(!project){

        project = new ProjectModel({
            date: new Date(),
            tasks: []
        });

        await project.save()
    }

    const task = new TaskModel({
        name: req.body.name,
        date: new Date(),
        projectId: project._id
    });

    // console.log('tasks', project.tasks)
    project.tasks = [...project.tasks,task._id]

    await project.save()

    await task.save().then(data => {

        res.send({
            message:"Task created successfully!!",
            task: data
        });

    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating task"
        });
    });
};

// Retrieve all tasks from the database.
exports.findAll = async (req, res) => {

    try {
        const tasks = await TaskModel.find();
        res.status(200).json(tasks);
    } catch(error) {
        res.status(404).json({message: error.message});
    }

};

// Find a single task with an id
exports.findOne = async (req, res) => {

    try {
        const task = await TaskModel.findById(req.params.id);
        res.status(200).json(task);
    } catch(error) {
        res.status(404).json({ message: error.message});
    }

};

// Update a task by the id in the request
exports.update = async (req, res) => {

    if(!req.body) {
        res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    await TaskModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false }).then(data => {
        if (!data) {
            res.status(404).send({
                message: `Task not found.`
            });
        }else{
            res.send({
                message: "Task updated successfully.",
                // task: data
            })
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

// Delete a task with the specified id in the request
exports.destroy = async (req, res) => {
    await TaskModel.findByIdAndDelete(req.params.id).then(data => {
        if (!data) {
            res.status(404).send({
                message: `Task not found.`
            });
        } else {
            res.send({
                message: "Task deleted successfully!"
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};
