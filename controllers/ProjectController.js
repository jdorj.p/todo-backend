const ProjectModel = require('../models/ProjectModel');

// Find a single project with an id
exports.findOne = async (req, res) => {
    try {
        const project = await ProjectModel.findById(req.params.id).populate('tasks')

        res.status(200).json(project);
    } catch(error) {

        console.log(error)

        res.status(404).json({ message: error.message});
    }
};

// Update a project by the id in the request
exports.update = async (req, res) => {
    if(!req.body) {
        res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    await ProjectModel.findByIdAndUpdate(id, {tasks: req.body.tasks}, { useFindAndModify: false }).then(data => {
        if (!data) {
            res.status(404).send({
                message: `Project not found.`
            });
        }else{
            res.send({ message: "Project updated successfully." })
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};
